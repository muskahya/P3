# THE  IODEF LIBRARY  

In this documentation, some instructions and usage of IODEF Library will be explained.   

  

## Getting Started:  

These instructions will get you a copy of the project up and running on your local machine for testing purpose.  The library consists of 10 classes and their attributes and functions.  

  

## Prerequisites:  

Python2, Flask and yaml

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install ```yaml``` and ```flask```.

```bash
pip install pyyaml
```

```bash
pip install flask

or

pip3 install flask
```
For Linux:

```bash
sudo apt-get install python-yaml
sudo yum install python-yaml
```

Or refer to:
[yaml installation](https://choosealicense.com/licenses/mit/), 
[Flask installation](http://flask.pocoo.org/docs/1.0/installation/)


## Running the Tests:  


To use our library we need some instructions. In this section, they will be explained. First, type ```python``` to your terminal and follow the commands below one by one or create a new python file and copy them in it. The commands is just to show the usage of classes and corresponding functions.  

  
```python
from iodef_lib import *
```

We have 10 different classes. The iodef class is a root class and it consists of other classes. With ```import *``` command above you may use any class of the library to create an object for a purpose.  

  
```python
a = iodef() 
```

The command above gives you an object of iodef class which is root class.  


```python
a.read_json(filename)
```

The ```read_json(...)``` function is a function that reads the json data from a “given” file if any. You should give the path of your file as a parameter to our function.  

 
```python
a.get_incident()[0].purpose  
```
The get_incident function consist of all incidents of the corresponding iodef object.  For instance ```a.get_incident()[0]``` gives you the first incident object of iodef object **'a'**. You then may call the attributes of the incident class object. In this case, we call the purpose which is the attribute of the incident class.  

  
```python
a.get_incident()[0].set_Contact(type="admin", role="creator")  
```
The logic is the same in this scenario. After getting the first incident class object of **'a'** which is iodef class object. You may call the function ```set_Contact(...)``` to set the contact information. 

```python
from datetime import datetime
a.get_incident()[0].set_GenerationTime(datetime.now())
```
The other function implemented also in incident class is ```set_GenerationTime(...)```. You may use that function to set the generation time of the incident that you are working with.   

  
```python
a.get_incident()[0].event.flow.system.node.location = ("52.04", "12.05")  
```
As you see above examples our implementation based on nested classes. Here is another example. You may reach location information by calling the parameters of classes and may easily set it.  

 
```python
a.get_incident()[0].event.flow.system.node.location[0]  
```

You may get the value of ```location[0]``` after assignment operation (previous command).  

  
```python
a.to_json()  
```

When you finish the operations on your iodef and the other nested classes, you may convert your iodef class object **'a'** to json. In our case ```dict()```. This command will give you a dictionary of the corresponding iodef message with all nested information.  

```python
a.to_string()
```
If you want to see your object as a string type, you may call ```to_string()``` function of iodef class.

## Create New Incidents
Until this point, we worked on the objects that we convert from json object which is read from a file. For the following commands, we will try to add our own incident object.   

   
```python
b = iodef()  

cont = contact("organization", "creator")  

id = incidentID("1","SA Node1")  

inc = incident(id,cont)  
```
To create a proper incident object you must have at least incidentID and contact information. That is why you have to have an object of both contact and incidentID classes. After having id and contact objects in a proper way like above you must assign them to the constructor of the incident object.  

 
```python
ad = additional("Power")  
```
To create a proper iodef object, in addition to the proper incident object you have to have at least the text attribute of the additional class.   

  
```python
b.add_incident(inc,ad)  
```

After that using ```add_incident(...)``` function you may have a new proper iodef object with its corresponding incidents and additional data.  

   
```python
b.to_json()  
```
Again if you want to convert the python object to json as a dictionary you may easily call ```to_json()``` function. 

### Unicoded Message:

In our library we have a new function to load from unicode or string message.

```python
c = iodef_lib()

text_msg = unicode("{'version': '2.0','xml:lang': 'en','Incident': [{'purpose': 'reporting','IncidentID': {'id':'1','name':'Distributed DoS'},'GenerationTime':'2018-11-20 08:36:43.585449+00:00','Contact': {'type': 'organization','role': 'creator'},'EventData': {'DetectTime':'2018-11-20 08:36:43.585449+00:00','Flow': {'System': {'category':'CISOC','Description':'CISOC','Node': {'Location': ['53.271802','-6.209862']}}}}}],'AdditionalData': {'name': 'CI','dtype': 'string','text': 'Energy'}}") 

c.load_from_unicode_or_string(text_msg)
```
The function above simply convert the unicoded message to our iodef object. You may send a string instead of unicode to the same function.

```python
c.to_json()
```
You may easily convert again our object to json as a dictionary. 

### Printing
```python
print(a)

print(b)

print(c)
```

If you want to see the content of your iodef class's objects in our cases they are **'a'**, **'b'** and **'c'**, You only call the ``` print(...)``` function.
