from iodef_lib import *
import json
import requests


b = iodef()
incc = incidentID("1", "2")
con = contact("mustafa", "kahya")
inc = incident(incc, con)
b.add_incident(inc,con)

bjson = b.to_json()



HEADERS = {
    'Content-Type': 'application/json'
}

R = requests.post('http://127.0.0.1:5000/todo/api/v1.0/iodefs', json = bjson, headers=HEADERS)

if R.ok:
    print("JSON: ", R.json())
else:
    print ("No proper request or url")