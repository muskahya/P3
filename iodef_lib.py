'''

@author: Kahya, Sabri Mustafa

'''

import json
import sys
import os.path
import ast
import yaml
import unicodedata
from json import JSONEncoder
from collections import OrderedDict


class iodef:
    """The root class"""

    def __init__(self):
        self.version = None
        self.xml = None
        self.assessment = None
        self.__incident = []
        self.additional = None
        self.d = None

    def load_from_unicode_or_string(self, text):
        self.d = text
        if type(self.d) == str:
            self.d = ast.literal_eval(self.d)
            
        elif type(self.d) == unicode:
            self.d = unicodedata.normalize('NFKD', self.d).encode('ascii','ignore')
            self.d = ast.literal_eval(self.d)
            print(type(self.d))
        else:
            raise ValueError("Error: Unvalid type, You should use type unicode or string")
            
        self.version = self.d['version']
        self.xml = self.d['xml:lang']
        self.assessment = self.d['assessment']
        for i in range(0, len(self.d['Incident'])):
            incID = incidentID(self.d['Incident'][i]['IncidentID']['id'],self.d['Incident'][i]['IncidentID']['name'])
            cont = contact(self.d['Incident'][i]['Contact']['type'], self.d['Incident'][i]['Contact']['role'])
            inc = incident(incID, cont)
            inc.purpose = self.d['Incident'][i]['purpose']
            inc.gtime = self.d['Incident'][i]['GenerationTime']
            inc.relatedActivity.incidentID.id = self.d['Incident'][i]['RelatedActivity']['IncidentID']['id']
            inc.relatedActivity.incidentID.name = self.d['Incident'][i]['RelatedActivity']['IncidentID']['name']
            inc.event.detecttime = self.d['Incident'][i]['EventData']['DetectTime']
            inc.event.flow.system.category = self.d['Incident'][i]['EventData']['Flow']['System']['category']
            inc.event.flow.system.description = self.d['Incident'][i]['EventData']['Flow']['System']['Description']
            inc.event.flow.system.node.location = self.d['Incident'][i]['EventData']['Flow']['System']['Node']['Location']
            for j in range(0, len(self.d['Incident'][i]['History'])):
                hist = history()
                histItem = historyItem()
                histItem.dateTime = self.d['Incident'][i]['History'][j]['HistoryItem']['DateTime']
                histItem.description = self.d['Incident'][i]['History'][j]['HistoryItem']['Description']
                histItem.action = self.d['Incident'][i]['History'][j]['HistoryItem']['action']
                histItem.extAction = self.d['Incident'][i]['History'][j]['HistoryItem']['ext-action']
                for l in range(0, len(self.d['Incident'][i]['History'][j]['HistoryItem']['AdditionalData'])):
                    addi = additional(self.d['Incident'][i]['History'][j]['HistoryItem']['AdditionalData'][l]['text'])
                    addi.name = self.d['Incident'][i]['History'][j]['HistoryItem']['AdditionalData'][l]['name']
                    addi.dtype = self.d['Incident'][i]['History'][j]['HistoryItem']['AdditionalData'][l]['dtype']
                    histItem.additional.append(addi)
                hist.historyItem = histItem
                inc.history.append(hist)
            self.__incident.append(inc)
        add = additional(self.d['AdditionalData']['text'])
        add.name = self.d['AdditionalData']['name']
        add.dtype = self.d['AdditionalData']['dtype']
        self.additional = add
 
        
    def to_json(self):
        """Convert python object to json format in our case it converts object to dict()"""
        return yaml.safe_load(json.dumps(self.reprJSON(), cls=ComplexEncoder, indent=3))
    
    def to_string(self):
        return json.dumps(self,cls=ComplexEncoder)

    def get_incident(self):
        return self.__incident

    def add_incident(self, inc, add):
        """Adds new incident with considering the attributes that have to be in any incident"""
        self.__incident.append(inc)
        self.additional = add

    def reprJSON(self):
        return OrderedDict([('version', self.version), ('xml:lang',self.xml), ('assessment', self.assessment), ('Incident', self.__incident), ('AdditionalData',self.additional)])

       
    def __str__(self):
        return json.dumps(self.reprJSON(), cls=ComplexEncoder, indent=3)
        


class incident:
    """Class for incident objects"""
    def __init__(self, inc, cont):
        self.purpose = None
        
        if isinstance(inc, incidentID):
            self.incidentID = inc
        else:
            raise ValueError("You must assign the incidentID class object as a parameter to incident class object's constructor")

        if isinstance(cont, contact):
            self.contact = cont
        else:
            raise ValueError("You must assign the contact class object as a parameter to incident class object's constructor")

        self.gtime = None
        self.event = event()
        self.history = []
        self.relatedActivity = relatedActivity()

    def set_Contact(self, type, role):
        """Sets the contacs (type, role) of iodef object's incident"""
        self.contact.type = type
        self.contact.role = role

    def set_GenerationTime(self, time):
        """Sets the generation time of iodef object's incident"""
        self.gtime = str(time)

    def reprJSON(self):
        return OrderedDict([('purpose',self.purpose), ('IncidentID',self.incidentID), ('Contact',self.contact), ('GenerationTime',self.gtime),
                    ('EventData',self.event), ('History', self.history), ('RelatedActivity',self.relatedActivity)])

class history:
    def __init__(self):
        self.historyItem = historyItem()
        
    def reprJSON(self):
        return OrderedDict([('HistoryItem', self.historyItem)])

        

class historyItem:
    def __init__(self):
        self.additional = []
        self.dateTime = None
        self.description = None
        self.action = None
        self.extAction = None
    def reprJSON(self):
        return OrderedDict([('AdditionalData', self.additional), ('DateTime', self.dateTime), ('Description', self.description), ('action', self.action), ('ext-action', self.extAction)])        
    

class relatedActivity:
    def __init__(self):
        self.incidentID = incidentIDOptional()
    
    def reprJSON(self):
        return OrderedDict([('IncidentID', self.incidentID)])
        
class incidentIDOptional:
    """Class for incidentID objects"""
    def __init__(self):
        self.id = None
        self.name = None

    def reprJSON(self):
        return OrderedDict([('id',self.id), ('name',self.name)])  
    
class incidentID:
    """Class for incidentID objects"""
    def __init__(self, id, name):
        self.id = id
        self.name = name

    def reprJSON(self):
        return OrderedDict([('id',self.id), ('name',self.name)])


class contact:
    """Class for contact objects"""
    def __init__(self, typ, role):
        self.type = typ
        self.role = role

    def reprJSON(self):
        return OrderedDict([('type',self.type), ('role',self.role)])


class event:
    def __init__(self):
        self.detecttime = None
        self.flow = flow()

    def reprJSON(self):
        return OrderedDict([('DetectTime',self.detecttime), ('Flow',self.flow)])


class flow:
    def __init__(self):
        self.system = system()

    def reprJSON(self):
        return OrderedDict([('System',self.system)])
                

class system:
    def __init__(self):
        self.category = None
        self.description = None
        self.node = node()

    def reprJSON(self):
        return OrderedDict([('category',self.category), ('Description',self.description), ('Node',self.node)])


class node:
    def __init__(self):
        self.location = None

    def reprJSON(self):
        return OrderedDict([('Location',self.location)])


class additional:
    """Class for AdditionalData"""
    def __init__(self, text):
        self.text = text
        self.dtype = "string"
        self.name = "CS"

    def reprJSON(self):
        return OrderedDict([('name',self.name), ('dtype',self.dtype), ('text',self.text)])


class ComplexEncoder(json.JSONEncoder):
    """Class for python object to json object conversion"""
    def default(self, obj):
        if hasattr(obj, 'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)
