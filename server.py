#Run the script as;
# $ export FLASK_APP=server.py
# $ flask run

from flask import Flask, jsonify, render_template, request
import json
import requests

app = Flask(__name__)

iodefs = [] 


@app.route('/todo/api/v1.0/iodefs', methods=['POST'])
def create_task():
    print(request)
    r = request.json
    print(r)
    #iodefs.append(r)
    if not request.json:
        abort(400)

    return jsonify(r), 201


if __name__ == '__main__':
    app.run(debug=True)
